External tools collection to save installation effort

bin/clc: AMD OpenCL front-end
bin/llvm3.1/: AMD HSAIL Backend binaries (LLVM 3.1 based)
bin/libHSAIL/hsailasm: from
    https://github.com/HSAFoundation/HSAIL-Tools
    branch hsail0.98plus
    commit 2fc4cf7cd4 on Oct 31, 2013
